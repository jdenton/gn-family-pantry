app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

$routeProvider.
  // /logentry
  when('/logentry', {
    templateUrl: '/views/logentry.html',
    controller: 'LogentryCtrl'
  }).

  // /login
  when('/login', {
    templateUrl: '/views/login.html',
    controller: 'LoginCtrl'
  }).

  // /members
  when('/members', {
    templateUrl: '/views/members.html',
    controller: 'MembersCtrl'
  }).

  // /members
  when('/members-view/:id', {
    templateUrl: '/views/members-view.html',
    controller: 'MembersViewCtrl'
  }).

  // /admin
  when('/admins', {
    templateUrl: '/views/admins.html',
    controller: 'AdminsCtrl'
  }).

  // /reports
  when('/reports', {
    templateUrl: '/views/reports.html',
    controller: 'ReportsCtrl'
  }).

  otherwise({ redirectTo: '/logentry' });

// HTML 5 Mode
//$locationProvider.html5Mode(true);

}]);
