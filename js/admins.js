app.controller('AdminsCtrl', function ($scope, $http, Backand, $filter)
{
  $scope.admin = {};
  $scope.showform = false;
  $scope.isedit = false;
  $scope.pagetitle = 'Add new administrator';

  //
  // Get admin records
  //
  $scope.get_admins = function()
  {
    // Order by LastName
    var params = {
      params: { sort: '[{fieldName:\'LastName\', order:\'asc\'}]' }
    };

    $http.get(Backand.getApiUrl() + '/1/objects/admins', params).success(function (json) {
      $scope.admins = json.data;
    });
  };

  //
  // Save admin record
  //
  $scope.save = function()
  {
    // Date
    $scope.admin.DateAdded = $filter('date')(new Date(), 'MM/dd/yyyy');

    // Send to Backand
    if ( $scope.isedit )
    {
      $http.put(Backand.getApiUrl() + '/1/objects/admins/' + $scope.admin.id, $scope.admin).success(function (json) {
        $scope.get_admins();
        $scope.reset();
      });
    } else {
      $http.post(Backand.getApiUrl() + '/1/objects/admins', $scope.admin).success(function (json) {
        $scope.get_admins();
        $scope.reset();
      });
    }
  }

  //
  // Delete record
  //
  $scope.delete = function( userid )
  {
    $http.delete(Backand.getApiUrl() + '/1/objects/admins/' + userid).success(function (json) {
        $scope.get_admins();
    });
  }

  //
  // Edit record
  //
  $scope.edit = function( row )
  {
    $scope.admin = row;
    $scope.showform = true;
    $scope.isedit = true;
    $scope.pagetitle = 'Edit administrator';
  }

  $scope.reset = function()
  {
    $scope.isedit = false;
    $scope.showform = false;
    $scope.admin = {};
    $scope.pagetitle = 'Add new administrator';
  }

  $scope.get_admins();
});
