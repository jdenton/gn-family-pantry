app.controller('MembersCtrl', function ($scope, $http, Backand, $filter)
{
  $scope.member = {};
  $scope.showform = false;
  $scope.isedit = false;
  $scope.pagetitle = 'Add new member';

  //
  // Get admin records
  //
  $scope.get_members = function()
  {
    // Order by LastName
    var params = {
      params: { sort: '[{fieldName:\'LastName\', order:\'asc\'}]' }
    };

    $http.get(Backand.getApiUrl() + '/1/objects/members', params).success(function (json) {
      $scope.members = json.data;
    });
  };

  //
  // Save admin record
  //
  $scope.save = function()
  {
    // Send to Backand
    if ( $scope.isedit )
    {
      $http.put(Backand.getApiUrl() + '/1/objects/members/' + $scope.member.id, $scope.member).success(function (json) {
        $scope.get_members();
        $scope.reset();
      });
    } else {
      $scope.member.DateAdded = $filter('date')(new Date(), 'MM/dd/yyyy');
      $http.post(Backand.getApiUrl() + '/1/objects/members', $scope.member).success(function (json) {
        $scope.get_members();
        $scope.reset();
      });
    }
  }

  //
  // Delete record
  //
  $scope.delete = function( userid )
  {
    $http.delete(Backand.getApiUrl() + '/1/objects/members/' + userid).success(function (json) {
        $scope.get_members();
    });
  }

  //
  // Edit record
  //
  $scope.edit = function( row )
  {
    $scope.member = row;
    $scope.showform = true;
    $scope.isedit = true;
    $scope.pagetitle = 'Edit member';
  }

  $scope.reset = function()
  {
    $scope.isedit = false;
    $scope.showform = false;
    $scope.member = {};
    $scope.pagetitle = 'Add new member';
  }

  $scope.get_members();
});
