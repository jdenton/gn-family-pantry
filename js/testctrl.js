app.controller('TestCtrl', function ($scope, $http, Backand)
{
  $scope.get_data = function()
  {
    $http.get(Backand.getApiUrl() + '/1/objects/admins').success(function (json) {
      console.log(json);

    });
  }

  $scope.save = function()
  {
    var fields = {
      FirstName: $scope.firstname,
      LastName: $scope.lastname
    };

    $http.post(Backand.getApiUrl() + '/1/objects/admins', fields).success(function (json) {
      console.log(json);

    });
  }

  window.addEventListener('online',  function() {
    console.log('online!!!');
  });
  window.addEventListener('offline', function() {
    console.log('offline :-(');
  });

  $scope.get_data();
});
