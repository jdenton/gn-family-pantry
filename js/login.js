app.controller('LoginCtrl', function ($scope, $http, Backand, $rootScope)
{
  // Initially set to logged out
  chrome.storage.local.set({ 'loggedin': false });
  $scope.login_error = false;
  $scope.login_error_message = '';

  $scope.login = function()
  {
    var params = {
      params: {
        filter: [
        {
          fieldName: 'UserName',
          operator: 'equals',
          value: $scope.UserName
        },
        {
          fieldName: 'Password',
          operator: 'equals',
          value: $scope.Password
        }
      ]
      }
    };

    $http.get(Backand.getApiUrl() + '/1/objects/admins', params).success(function (json) {
      if ( json.totalRows > 0 )
      {
        // We have an admin user. Set to logged in.
        chrome.storage.local.set({ 'loggedin': true });

        // Redirect to /members
        window.location.href = '#/members';
      } else {
        $scope.login_error = true;
        $scope.login_error_message = 'No administrator was found with that User Name & Password!';
      }
    });
  };
});
