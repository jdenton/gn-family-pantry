function onError(e) {
  console.log(e);
}

// FILESYSTEM SUPPORT ----------------------------------------------------------
var fs = null;
var FOLDERNAME = 'test';

function writeFile(blob) {
  if (!fs) {
    return;
  }

  fs.root.getDirectory(FOLDERNAME, {create: true}, function(dirEntry) {
    dirEntry.getFile(blob.name, {create: true, exclusive: false}, function(fileEntry) {
      // Create a FileWriter object for our FileEntry, and write out blob.
      fileEntry.createWriter(function(fileWriter) {
        fileWriter.onerror = onError;
        fileWriter.onwriteend = function(e) {
          console.log('Write completed.');
        };
        fileWriter.write(blob);
      }, onError);
    }, onError);
  }, onError);
}
// -----------------------------------------------------------------------------

var app = angular.module('fp', [
  'ngRoute',
  'backand',
  'ngMaterial'
]);

app.run(function($rootScope) {
  chrome.storage.local.set({ 'loggedin': false });
});

app.config(function (BackandProvider) {
  BackandProvider.setAppName('familypantry');
  BackandProvider.setSignUpToken('c2a70ac7-5eed-4e86-a557-c91ff9e7e6e6');
  BackandProvider.setAnonymousToken('08e013f2-38c5-4147-aa4c-d843f3a7a06e');
});

app.config( ['$compileProvider', function( $compileProvider )
{
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|blob:chrome-extension):/);
}
]);

app.config(function ( $mdThemingProvider ) {
  $mdThemingProvider.theme('default')
    .primaryPalette('indigo', {
      'default': '400',
      'hue-1': '100',
      'hue-2': '600',
      'hue-3': 'A100'
    })
    .accentPalette('green', {
      'default': '400',
      'hue-1': '100',
      'hue-2': '600',
      'hue-3': 'A100'
    })
    .warnPalette('red');
});
