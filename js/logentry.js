app.controller('LogentryCtrl', function ($scope, $http, Backand, $filter, $timeout, $rootScope)
{
  $scope.searchText = '';
  $scope.log = {};
  var log = {};

  chrome.storage.local.get('loggedin', function (result) {
    $scope.loggedin = result.loggedin;
  });

  $scope.can_save = true;
  $scope.saved = false;
  $scope.save_error_message = '';

  $scope.searchTextChange = function( str )
  {
    $scope.searchText = str;
  };

  // Save our record
  $scope.save = function()
  {
    if ( $scope.log.Member == null )
    {
      $scope.can_save = false;
      $scope.save_error_message = 'You must select your NAME.';
    } else if ( $scope.log.Hours == null )
    {
      $scope.can_save = false;
      $scope.save_error_message = 'You must select your number of HOURS.';
    } else if ( $scope.log.FoodAmount == null )
    {
      $scope.can_save = false;
      $scope.save_error_message = 'You must select your FOOD AMOUNT.';
    } else {
      $scope.can_save = true;
    }

    if ( $scope.can_save == true )
    {
      log.DateAdded = $filter('date')(new Date(), 'MM/dd/yyyy');
      log.MembersId = $scope.log.Member.id;
      log.Hours = $scope.log.Hours;
      log.FoodAmount = $scope.log.FoodAmount;

      // Did this user already put an entry in for today?
      var params = {
        params: { filter: "[{fieldName:'DateAdded', operator:'equals', value:'2016-03-29T07:00:00.000Z'}]" }
      };
      $http.get(Backand.getApiUrl() + '/1/objects/memberlog', params).success(function (json) {
        var existing_entry = json.data;
      });

      $http.post(Backand.getApiUrl() + '/1/objects/memberlog', log).success(function (json) {
        $scope.saved = true;
        $scope.reset();
      });
    }
  };

  $scope.reset = function()
  {
    $scope.log.Hours = null;
    $scope.log.FoodAmount = null;
    $scope.log.Member = null;
    $scope.searchText = '';
    $timeout(function() {
      $scope.saved = false;
    }, 5000);
  }

  // Populate our form
  $scope.hours = [1,2,3,4,5,6,7,8,9,10];
  $scope.food = [];
  for ( a = 1; a <= 30; a++ )
  {
    $scope.food.push(a);
  }

  // Get all of our members.
  $scope.get_members = function()
  {
    // Order by LastName
    var params = {
      params: { sort: '[{fieldName:\'LastName\', order:\'asc\'}]' }
    };

    $http.get(Backand.getApiUrl() + '/1/objects/members', params).success(function (json) {
      $scope.members = json.data;
      angular.forEach($scope.members, function(m) {
        m.FullName = m.FirstName + ' ' + m.LastName;
      });
    });
  };

  $scope.get_members();
});
