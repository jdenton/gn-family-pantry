app.controller('MembersViewCtrl', function ($scope, $routeParams, $http, Backand, $filter)
{
  // Get member
  $scope.get_member = function()
  {
    $http.get(Backand.getApiUrl() + '/1/objects/members/' + $routeParams.id).success(function (json) {
      $scope.member = json;

      // Get hours records for this member
      var params = {
        params: {
          filter: "[{fieldName:'MembersId', operator:'equals', value:" + $routeParams.id + "}]",
          sort: "[{fieldName:'DateAdded',order:'desc'}]"
        }
      };
      $http.get(Backand.getApiUrl() + '/1/objects/memberlog', params).success(function (json) {
        $scope.memberhours = json.data;
        $scope.total_hours = 0;
        $scope.total_food = 0;
        angular.forEach(json.data, function(r) {
          $scope.total_hours += r.Hours;
          $scope.total_food += r.FoodAmount;
        });
      });
    });
  }

  $scope.get_member();
});
